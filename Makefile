COMMIT := $(shell git rev-parse HEAD)
VERSION ?= $(shell git describe --tags ${COMMIT})
IMG ?= registry.gitlab.com/emadolsky/littlebig:${VERSION}

CGO_ENABLED=0
GOOS=linux
GOARCH=amd64

mod:
	go mod tidy

build:
	CGO_ENABLED=${CGO_ENABLED} GOOS=${GOOS} GOARCH=${GOARCH} go build -o ./bin/littlebig ./cmd/littlebig/main.go

docker-build:
	docker build -t ${IMG} .

docker-push:
	docker push ${IMG}

run:
	CGO_ENABLED=${CGO_ENABLED} GOOS=${GOOS} GOARCH=${GOARCH} go run ./cmd/littlebig/main.go