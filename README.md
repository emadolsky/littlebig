# LittleBig
LittleBig is my 2-day project for interview which does url shortening and
handles analytics of url redirects.

LittleBig is written in go and is designed to be as much flexible as it can.
currently it is integrated with Redis as a cache layer and PostgreSQL as
main database and analytics collection.

## Design
The code-level design of LittleBig is mainly focused on flexibility and
modularity. All of the external components needed for LittleBig are abstract
enough that you can easily add support for different technologies.

The architecture side of the design for sakes of availability, scalability,
and performance is mostly considered and not implemented because of lack of
time. In-depth design considerations are available [here](./DESIGN.md)

## Running the project
In order to run the project, first you should decide about your configurations.
LittleBig top-level configurations are gathered in a [config file](config.yaml).
In this file, you have control over what components do you use for bringing
a url shortner server up. Currently you can use *PostgreSQL* or *Memory* as url
and user store, *PostgreSQL* as analytics collection, *BigCache* or *Redis* on
a layered manner as cache, and *RandomGenerator* as url shortner method.

> Note: If you chose to use PostgreSQL, initialize the database by importing 
> [this file](scripts/postgres/initialize.sql) into the database.

> Note: For PostgreSQL analytics aggregations, materialized views should be
> refreshed daily. Therefore
> [these commands](scripts/postgres/refresh_materialized_views.sql)
> should be run at the start of each day. A cronJob connecting to the database
> will do the trick.

After configuring as you wanted, you can easily run the application using:
```shell script
make run
```

## Deploy
Obviously, you can deploy the stack regularly on a host. But other deployment
methods are prepared to make it easy to deploy or demo the project.
 
### Docker
For ease of usage and demo of LittleBig, a
[docker-compose deployment](./deploy/docker) is ready for use. In the directory,
you can easily deploy LittleBig with a two-layered cache (BigCache-Redis)
and PostgreSQL database using:

```shell script
docker-compose up
```

## Benchmark
For benchmarking the shortner stack, Apache Benchmark (ab) is used. There are
other projects with a lot more features and capabilities (JMeter, wrk, etc.)
but I did not get time to learn and use them in the limited duration.

Details about benchmark and benchmark scripts are gathered in
[this place](./scripts/benchmark).

## TODOs
There are a few things that I wish I could have more time to do:
* Testing (unit, integration, e2e)
* Monitoring (i.e. exporter for cache hit rate, API calls, readiness)
* API Docs
* Refactoring analytics parts
* Better logging