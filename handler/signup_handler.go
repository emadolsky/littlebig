package handler

import (
	"encoding/json"
	"net/http"

	"littlebig/api"
	"littlebig/store"

	log "github.com/sirupsen/logrus"
)


type SignupHandler struct {
	userStore store.UserStore
}

func NewSignupHandler(userStore store.UserStore) Handler {
	return &SignupHandler{
		userStore: userStore,
	}
}

func (h *SignupHandler) Handle(responseWriter http.ResponseWriter, request *http.Request) {
	signupReq := &api.SignupRequest{}
	if err := json.NewDecoder(request.Body).Decode(signupReq); err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	newUser := store.User{
		Username:    signupReq.Username,
		Email:       signupReq.Email,
		Password:    getPasswordHash(signupReq.Password),
		PhoneNumber: signupReq.PhoneNumber,
	}
	if err := h.userStore.SaveUser(newUser); err != nil {
		log.WithError(err).Info("could not save user")
		// error is conflict
		responseWriter.WriteHeader(http.StatusConflict)
		return
	}
	responseWriter.WriteHeader(http.StatusCreated)
	return
}
