package handler

import (
	"encoding/json"
	"fmt"
	"net/http"

	"littlebig/api"
	"littlebig/shortner"
	"littlebig/store"

	log "github.com/sirupsen/logrus"
)

const maxGenUrlRetry = 5

type CreateUrlHandler struct {
	urlShortner shortner.Shortner
	urlStore    store.UrlStore
}

func NewCreateUrlHandler(urlShortner shortner.Shortner, urlStore store.UrlStore) Handler {
	return &CreateUrlHandler{
		urlShortner: urlShortner,
		urlStore:    urlStore,
	}
}

func (h *CreateUrlHandler) Handle(responseWriter http.ResponseWriter, request *http.Request) {
	createUrlRes := &api.CreateUrlResponse{}
	createUrlReq := &api.CreateUrlRequest{}
	if err := json.NewDecoder(request.Body).Decode(createUrlReq); err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	if createUrlReq.LongUrl == "" {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	var shortUrl string
	isCustom := false
	if createUrlReq.CustomUrl != nil {
		isCustom = true
	}

	userId, _ := getUserFromRequest(request)
	var err error
	if isCustom {
		shortUrl = *createUrlReq.CustomUrl
		if err = h.urlStore.SaveUrl(createUrlReq.LongUrl, shortUrl, userId); err != nil {
			if store.IsConflictError(err) {
				responseWriter.WriteHeader(http.StatusConflict)
				return
			} else {
				log.WithError(err).Error("error on saving url")
				responseWriter.WriteHeader(http.StatusInternalServerError)
				return
			}
		}
	} else {
		shortUrl, err = h.findAndSaveShortUrl(createUrlReq, userId)
		if err != nil {
			responseWriter.WriteHeader(http.StatusInternalServerError)
			return
		}
	}

	createUrlRes.ShortUrl = shortUrl
	responseWriter.WriteHeader(http.StatusCreated)
	marshalledRes, _ := json.Marshal(createUrlRes)
	responseWriter.Write(marshalledRes)
}

func (h *CreateUrlHandler) findAndSaveShortUrl(createUrlReq *api.CreateUrlRequest, ownerId int) (string, error) {
	for tryNum := 0; tryNum < maxGenUrlRetry; tryNum++{
		shortUrl, err := h.urlShortner.GenerateShortUrl(createUrlReq.LongUrl)
		if err != nil {
			return "", err
		}
		if err = h.urlStore.SaveUrl(createUrlReq.LongUrl, shortUrl, ownerId); err == nil {
			// successful save
			return shortUrl, nil
		}
		if store.IsConflictError(err) {
			// new url is needed
			continue
		} else {
			// if any error rather than conflict happens, we return error
			log.WithError(err).Error("unexpected error on saving url")
			return "", err
		}
	}
	// could not generate correct url after retries
	log.Error("maximum number of url generation reached")
	return "", fmt.Errorf("maximum number of url generation reached")
}