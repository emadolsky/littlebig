package handler

import (
	"encoding/json"
	"net/http"

	"littlebig/analytics"
	"littlebig/api"
	"littlebig/cache"
	"littlebig/store"

	log "github.com/sirupsen/logrus"
)

type AnalyticsHandler struct {
	urlStore           store.UrlStore
	urlCache           cache.UrlCache
	analyticsRetriever analytics.AnalyticsRetriever
}

func NewAnalyticsHandler(urlStore store.UrlStore, dataCollector analytics.AnalyticsRetriever) Handler {
	return &AnalyticsHandler{
		urlStore:           urlStore,
		analyticsRetriever: dataCollector,
	}
}

type AnalyticsRequest struct {
	ShortUrl    string                       `json:"shortUrl"`
	ClientCount bool                         `json:"clientCount"`
	ByBrowser   bool                         `json:"byBrowser"`
	ByDevice    bool                         `json:"byDevice"`
	TimeRange   analytics.AnalyticsTimeRange `json:"timeRange"`
}

func (h *AnalyticsHandler) Handle(responseWriter http.ResponseWriter, request *http.Request) {
	userId, _ := getUserFromRequest(request)
	analyticsReq := &AnalyticsRequest{}
	if err := json.NewDecoder(request.Body).Decode(analyticsReq); err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	_, ownerId, err := h.urlStore.LoadUrl(analyticsReq.ShortUrl)
	if err != nil {
		if store.IsNotFoundError(err) {
			responseWriter.WriteHeader(http.StatusNotFound)
			return
		}
		log.WithError(err).Error("could not load url from store")
		responseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}
	if ownerId != userId {
		responseWriter.WriteHeader(http.StatusForbidden)
		return
	}
	var aggrs []analytics.FinalAggregatedAnalytics
	if analyticsReq.ClientCount {
		if analyticsReq.ByBrowser {
			aggrs, err = h.analyticsRetriever.GetClientCountByBrowserType(analyticsReq.ShortUrl, analyticsReq.TimeRange)
		} else if analyticsReq.ByDevice {
			aggrs, err = h.analyticsRetriever.GetClientCountByDeviceType(analyticsReq.ShortUrl, analyticsReq.TimeRange)
		} else {
			aggrs, err = h.analyticsRetriever.GetTotalClientCount(analyticsReq.ShortUrl, analyticsReq.TimeRange)
		}
	} else {
		if analyticsReq.ByBrowser {
			aggrs, err = h.analyticsRetriever.GetUrlHitCountByBrowser(analyticsReq.ShortUrl, analyticsReq.TimeRange)
		} else if analyticsReq.ByDevice {
			aggrs, err = h.analyticsRetriever.GetUrlHitCountByDeviceType(analyticsReq.ShortUrl, analyticsReq.TimeRange)
		} else {
			aggrs, err = h.analyticsRetriever.GetTotalUrlHitCount(analyticsReq.ShortUrl, analyticsReq.TimeRange)
		}
	}
	if err != nil {
		log.WithError(err).Error("could not load analytics from collection")
		responseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}
	response := &api.AnalyticsResponse{
		Analytics: make([]api.SingleAnalyticsResponse, len(aggrs)),
	}
	for i, singleAggr := range aggrs {
		response.Analytics[i] = api.SingleAnalyticsResponse{
			ShortUrl: singleAggr.ShortUrl,
			Browser:  singleAggr.Browser,
			Device:   singleAggr.Device,
			Count:    singleAggr.Count,
		}
	}
	responseWriter.WriteHeader(http.StatusOK)
	marshalledRes, _ := json.Marshal(response)
	responseWriter.Write(marshalledRes)
}
