package handler

import (
	"encoding/json"
	"net/http"
	"time"

	"littlebig/api"
	"littlebig/config"
	"littlebig/store"

	"github.com/dgrijalva/jwt-go"
	log "github.com/sirupsen/logrus"
)


type LoginHandler struct {
	userStore store.UserStore
}

func NewLoginHandler(userStore store.UserStore) Handler {
	return &LoginHandler{
		userStore: userStore,
	}
}

func (h *LoginHandler) Handle(responseWriter http.ResponseWriter, request *http.Request) {
	loginReq := &api.LoginRequest{}
	if err := json.NewDecoder(request.Body).Decode(loginReq); err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}
	var user *store.User
	var err error
	if user, err = h.userStore.LoadUser(loginReq.Username); err != nil {
		if store.IsNotFoundError(err) {
			responseWriter.WriteHeader(http.StatusUnauthorized)
			return
		}
		responseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}
	if user.Password != getPasswordHash(loginReq.Password) {
		responseWriter.WriteHeader(http.StatusUnauthorized)
		return
	}

	tokenString, err := generateJwtToken(loginReq, user.ID)
	if err != nil {
		responseWriter.WriteHeader(http.StatusInternalServerError)
		log.WithError(err).Error("could not sign jwt key")
		return
	}

	loginRes := &api.LoginResponse{
		Token: tokenString,
	}
	responseWriter.WriteHeader(http.StatusOK)
	marshalledRes, _ := json.Marshal(loginRes)
	responseWriter.Write(marshalledRes)
}

func generateJwtToken(loginReq *api.LoginRequest, userId int) (string, error) {
	expirationTime := time.Now().Add(jwtExpirationDuration)
	claims := &Claims{
		Username: loginReq.Username,
		UserId:   userId,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(config.JwtKey))
}