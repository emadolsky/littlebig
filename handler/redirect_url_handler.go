package handler

import (
	"net/http"

	"littlebig/analytics"
	"littlebig/cache"
	"littlebig/store"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

type RedirectUrlHandler struct {
	urlStore      store.UrlStore
	urlCache      cache.UrlCache
	dataCollector analytics.DataCollector
}

func NewRedirectUrlHandler(urlStore store.UrlStore, urlCache cache.UrlCache, dataCollector analytics.DataCollector) Handler {
	return &RedirectUrlHandler{
		urlStore:      urlStore,
		urlCache:      urlCache,
		dataCollector: dataCollector,
	}
}

func (h *RedirectUrlHandler) Handle(responseWriter http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	shortUrl := vars["shortUrl"]
	if h.urlCache != nil {
		cachedLongUrl, err := h.urlCache.GetUrl(shortUrl)
		if err == nil {
			http.Redirect(responseWriter, request, cachedLongUrl, http.StatusSeeOther)
			h.dataCollector.UrlHit(analytics.ExtractAgentDataFromRequest(shortUrl, request))
			return
		}
		log.WithError(err).WithField("shortUrl", shortUrl).Info("cache missed")
	}
	longUrl, _, err := h.urlStore.LoadUrl(shortUrl)
	if err != nil {
		if store.IsNotFoundError(err) {
			responseWriter.WriteHeader(http.StatusNotFound)
			return
		}
		log.WithError(err).Error("error on loading url")
		responseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}
	http.Redirect(responseWriter, request, longUrl, http.StatusSeeOther)
	h.dataCollector.UrlHit(analytics.ExtractAgentDataFromRequest(shortUrl, request))
	go func() {
		err := h.urlCache.SetUrl(shortUrl, longUrl)
		if err != nil {
			log.WithError(err).Error("setting cache failed")
		}
	}()
}
