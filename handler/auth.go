package handler

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const jwtExpirationDuration = time.Minute * 5

type Claims struct {
	jwt.StandardClaims
	Username string `json:"username"`
	UserId   int    `json:"userId"`
}

func getUserFromRequest(request *http.Request) (userId int, username string) {
	contextUser := request.Context().Value("user")
	contextUserClaimMap := contextUser.(*jwt.Token).Claims.(jwt.MapClaims)
	username = contextUserClaimMap["username"].(string)
	// What???
	userId = int(contextUserClaimMap["userId"].(float64))
	return
}

func getPasswordHash(password string) string {
	hashBytes := md5.Sum([]byte(password))
	passwordHashStr := fmt.Sprintf("%x", hashBytes)
	return passwordHashStr
}
