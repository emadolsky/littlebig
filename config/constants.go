package config

import "time"

const (
	JwtKey                     = "Em@D's 1 aNd 0n1y pa$sw"
	CharSet                    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_"
	PostgresOperationTimeout   = time.Second
	RedisCacheOperationTimeout = time.Millisecond * 40
)
