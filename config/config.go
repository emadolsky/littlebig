package config

import (
	"flag"
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type LittleBigConfig struct {
	UrlStore       UrlStoreConfig       `yaml:"urlStore"`
	AnalyticsStore AnalyticsStoreConfig `yaml:"analyticsStore"`
	UrlShortner    UrlShortnerConfig    `yaml:"urlShortner"`
	CacheLayers    []CacheLayerConfig   `yaml:"cacheLayers"`
	ListenAddress  string				`yaml:"listenAddress"`
}

type CacheLayerConfig struct {
	BigCache *BigCacheConfig `yaml:"bigCache"`
	Redis    *RedisConfig    `yaml:"redis"`
}

type BigCacheConfig struct {
	TTLSeconds int `yaml:"ttlSeconds"`
}

type RedisConfig struct {
	TTLSeconds int                `yaml:"ttlSeconds"`
	Shards     []RedisShardConfig `yaml:"shards"`
}

type RedisShardConfig struct {
	Name    string `yaml:"name"`
	Address string `yaml:"address"`
}

type UrlShortnerConfig struct {
	Random *RandomUrlShortnerConfig `yaml:"random"`
}

type RandomUrlShortnerConfig struct {
	Length int `yaml:"length"`
}

type UrlStoreConfig struct {
	Postgres *PostgresStoreConfig  `yaml:"postgres,omitempty"`
	Memory   *MemoryUrlStoreConfig `yaml:"memory,omitempty"`
}

type AnalyticsStoreConfig struct {
	Postgres *PostgresAnalyticsStore `yaml:"postgres,omitempty"`
}

type PostgresAnalyticsStore struct {
	PostgresStoreConfig `yaml:",inline"`
	SyncIntervalSeconds int `yaml:"syncIntervalSeconds"`
}

type PostgresStoreConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Database string `yaml:"database"`
	Password string `yaml:"password"`
}

type MemoryUrlStoreConfig struct {
}

var (
	configPath string
)

func init() {
	flag.StringVar(&configPath, "configPath", "config.yaml", "path to configuration file")
}

func Setup() {
	flag.Parse()
}

func Extract() (*LittleBigConfig, error) {
	yamlFile, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		log.WithError(err).Error("could not read config file")
		return nil, err
	}
	mainConfig := &LittleBigConfig{}
	err = yaml.Unmarshal(yamlFile, mainConfig)
	if err != nil {
		log.WithError(err).Error("could not parse config file")
		return nil, err
	}
	return mainConfig, nil
}
