module littlebig

go 1.12

require (
	github.com/allegro/bigcache v1.2.1
	github.com/auth0/go-jwt-middleware v0.0.0-20200810150920-a32d7af194d1
	github.com/avct/uasurfer v0.0.0-20191028135549-26b5daa857f1
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-redis/cache/v8 v8.0.0-beta.14
	github.com/go-redis/redis/v8 v8.0.0-beta.11
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.8.0
	github.com/sirupsen/logrus v1.6.0
	gopkg.in/yaml.v2 v2.3.0
)
