# LittleBig Architecture
LittleBig's main duty is to shorten urls, redirect shortened urls, and
keep and provide analytics data about the redirects. In order to make it
scalable and getting good performance some considerations have been done
that I try to point them out in this document.

# LittleBig Main Parts
## URL Shortening
The approach of shortening the urls in LittleBig is to remain flexible to
different methods of shortenings.

Current method used for shortening the URLs is random generation. This
method has some pros:
* The implementation of random generated strings is so simple.
* The shortened URLs do not have any relation to the actual URL. It means that
from seeing the short URL you cannot have any guesses about the actual URL and
vice versa.
* If some collision happens (which is more possible if length of short urls
are low), repeat in the procedure of generating the url is enough. No special
logic is needed.

## Caching
Caching can be done in different layers: 
* One notable cache layer, is the in-memory cache of backends themselves.
These solutions are so fast since the backend does not need to travel over
network to reach the data. everything is local and fast.
* Since each of these backend replicas are subject to restarts (because of
new deployment, lack of space, errors, etc.) and losing cache can cause
big performance drops, an external in-memory cache solution is suitable.
Also, one property of this type of caching is that cache is shared between
backends.

With these in mind, LittleBig supports multi-layer caching. The implementation
is sequential right now (lookup layers are in order) but this can also be done
concurrently to perform better when first layers miss the cache.

Currently supported types of cache are [BigCache](https://github.com/allegro/bigcache)
and [Redis](https://redis.io/).

## Database
LittleBig needs persistence of data because users, URLs, and user actions
need to be stored for future use. Therefore having database for each of these
purposes seems crucial. But there are differences between kinds of these data:
1. URLs and Users data are objects or structures that are mainly looked up by one
identifier. They need fast fetches but the amount of data in processes is low.
Therefore a Key-Value Database, a Document-based Database, or a RDBMS(which might
have scalability difficulties comparing to other mentioned solutions) can fulfill
our needs.
2. User actions (redirects) and generally analytics data are different. The
amount of data is much more than the above data and lots of processing over
different attributes of structures might be needed; where OLAP solutions are
good answers (A good example for LittleBig case, which needs to get
statistics over time is [Apache Druid](https://github.com/apache/druid);
An OLAP database with a lot of complex analytics features and time-series
support).

In LittleBig design lots of solutions considered in each part. In the end,
PostgreSQL for both parts where selected. The reasons of these selection are:
* I have experience with SQL databases and more with PostgreSQL in particular.
* I have little experience with document-based databases and persistent key-value
databases.
* High ingestion rate on analytics could be handled in backend with stacking
data and then ingesting larger bulks in the RDBMS. Also, aggregations on the
large amount of data can be handled async on materialized views because of
well-defined and limited time-ranges.

Also, for read scalability, PostgreSQL supports read standbys which backend
can balance loads on them. For write scalability sharding is needed which
is achievable in backend or with use of third-party projects.

# LittleBig, For The Best 
## Achieving High Availability
All of the components in the current state of LittleBig can get high-available.
* backend is stateless so there is no worries on deploying multiple instances
of it.
* Redis can be clustered with replication and solutions like
[redis sentinel](https://redis.io/topics/sentinel) can be deployed for achieving
high availability on both read and write.
* PostgreSQL supports replication (primary/standby) so read can be high
available easily. But for write, failover solutions are needed which native
PostgreSQL does not support. Third-party projects like 
[Patroni](https://github.com/zalando/patroni) can do the job.

## Scaling Performance
To be a good solution at being under a high load, LittleBig can perform well:
* As mentioned above, multiple replicas of backend can be deployed. This gives
us horizontal scalability in backend layer
* Caching layer supports sharding which helps write scalability. Redis cache layer in LittleBig can have multiple
shards. Therefore multiple clusters of Redis can be used instead of one big cluster.
Sharding happens in backend layer. For read scalability, replication is enough.
* PostgreSQL layer can have read replicas of the primary instance. Support for
load balancing read queries between multiple instances is not too hard but is not
implemented because I am not sure yet if it is really needed. But anyways,
This can help on read side. On write side, things are more difficult and not implemented.
But a good solution might be sharding or even using an alternative which
has better support on horizontal scalability.