package api

type AnalyticsResponse struct {
	Analytics []SingleAnalyticsResponse `json:"analytics"`
}

type SingleAnalyticsResponse struct {
	ShortUrl string `json:"shortUrl"`
	Browser  *int   `json:"browser,omitempty"`
	Device   *int   `json:"device,omitempty"`
	Count    int    `json:"count"`
}