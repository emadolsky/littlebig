package api

type CreateUrlRequest struct {
	LongUrl   string  `json:"longUrl"`
	CustomUrl *string `json:"customUrl,omitempty"`
}

type CreateUrlResponse struct {
	ShortUrl string `json:"shortUrl,omitempty"`
}
