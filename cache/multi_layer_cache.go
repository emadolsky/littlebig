package cache

import (
	"fmt"
	"strings"
)

type UrlMultiLayerCache struct {
	layers []UrlCache
}

func NewUrlMultiLayerCache(layers []UrlCache) *UrlMultiLayerCache {
	return &UrlMultiLayerCache{
		layers: layers,
	}
}

func (c *UrlMultiLayerCache) GetUrl(shortUrl string) (string, error) {
	for _, layer := range c.layers {
		// TODO: lookups can be concurrent
		shortUrl, err := layer.GetUrl(shortUrl)
		if err == nil {
			return shortUrl, nil
		}
	}
	return "", fmt.Errorf("all layers missed the cache")
}

func (c *UrlMultiLayerCache) SetUrl(shortUrl, longUrl string) error {
	var errors []string
	for _, layer := range c.layers {
		err := layer.SetUrl(shortUrl, longUrl)
		if err != nil {
			errors = append(errors, err.Error())
		}
	}
	if len(errors) == 0 {
		return nil
	}
	return fmt.Errorf(strings.Join(errors, " & "))
}
