package cache

import (
	"context"
	"time"

	"littlebig/config"

	rediscache "github.com/go-redis/cache/v8"
	"github.com/go-redis/redis/v8"
)

type UrlRedisCache struct {
	cache *rediscache.Cache
	ttl   time.Duration
}

type RedisCacheShard struct {
	Name    string
	Address string
}

func NewUrlRedisCache(shards []RedisCacheShard, cacheTTL time.Duration) (*UrlRedisCache, error) {
	addrs := make(map[string]string)
	for _, shard := range shards {
		addrs[shard.Name] = shard.Address
	}
	ring := redis.NewRing(&redis.RingOptions{
		Addrs: addrs,
	})
	cache := rediscache.New(&rediscache.Options{
		Redis:      ring,
	})
	return &UrlRedisCache{
		cache: cache,
		ttl:   cacheTTL,
	}, nil
}

func (c *UrlRedisCache) GetUrl(shortUrl string) (string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.RedisCacheOperationTimeout)
	defer cancel()
	var longUrl string
	err := c.cache.Get(ctx, shortUrl, &longUrl)
	if err != nil {
		return "", err
	}
	return longUrl, nil
}

func (c *UrlRedisCache) SetUrl(shortUrl, longUrl string) error {
	ctx, cancel := context.WithTimeout(context.Background(), config.RedisCacheOperationTimeout)
	defer cancel()
	if err := c.cache.Set(&rediscache.Item{
		Ctx:   ctx,
		Key:   shortUrl,
		Value: longUrl,
		TTL:   c.ttl,
	}); err != nil {
		return err
	}
	return nil
}
