package cache

type UrlCache interface {
	GetUrl(shortUrl string) (string, error)
	SetUrl(shortUrl, longUrl string) error
}
