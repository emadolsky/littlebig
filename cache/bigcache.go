package cache

import (
	"time"

	"github.com/allegro/bigcache"
)

type UrlBigCache struct {
	cache *bigcache.BigCache
}

func NewUrlBigCache(ttl time.Duration) (*UrlBigCache, error) {
	cache, err := bigcache.NewBigCache(bigcache.DefaultConfig(ttl))
	if err != nil {
		return nil, err
	}
	return &UrlBigCache{
		cache: cache,
	}, nil
}

func (c * UrlBigCache) GetUrl(shortUrl string) (string, error) {
	urlBytes, err := c.cache.Get(shortUrl)
	if err != nil {
		return "", err
	}
	return string(urlBytes), nil
}

func (c * UrlBigCache) SetUrl(shortUrl, longUrl string) error {
	err := c.cache.Set(shortUrl, []byte(longUrl))
	if err != nil {
		return err
	}
	return nil
}