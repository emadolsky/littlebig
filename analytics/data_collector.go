package analytics

import (
	"net/http"
	"strings"

	"github.com/avct/uasurfer"
)

type HitData struct {
	ShortUrl   string `json:"shortUrl"`
	Browser    int    `json:"browser,omitempty"`
	DeviceType int    `json:"deviceType,omitempty"`
	ClientAddr string `json:"client,omitempty"`
}

type AggregatedAnalyticsMap map[HitData]int

type FinalAggregatedAnalytics struct {
	ShortUrl string
	Browser  *int
	Device   *int
	Count    int
}

func aggregateHitData(hitDataList []HitData) AggregatedAnalyticsMap {
	aggr := make(AggregatedAnalyticsMap)
	for _, hitData := range hitDataList {
		aggr[hitData] += 1
	}
	return aggr
}

type DataCollector interface {
	UrlHit(data HitData)
}

func ExtractAgentDataFromRequest(shortUrl string, request *http.Request) HitData {
	userAgent := uasurfer.Parse(request.Header.Get("User-Agent"))
	var clientAddr string
	remoteAddr := request.RemoteAddr
	if remoteAddr[0] == '[' {
		s := strings.Split(remoteAddr, "]")[0]
		clientAddr = s[1:]
	} else {
		s := strings.Split(remoteAddr, ":")[0]
		clientAddr = s
	}
	return HitData{
		ShortUrl:   shortUrl,
		Browser:    int(userAgent.Browser.Name),
		DeviceType: int(userAgent.DeviceType),
		ClientAddr: clientAddr,
	}
}

type AnalyticsTimeRange int

const (
	AnalyticsTimeRangeDaily AnalyticsTimeRange = iota
	AnalyticsTimeRangeYesterday
	AnalyticsTimeRangeWeekly
	AnalyticsTimeRangeMonthly
)

type AnalyticsRetriever interface {
	GetTotalUrlHitCount(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error)
	GetUrlHitCountByBrowser(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error)
	GetUrlHitCountByDeviceType(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error)
	GetTotalClientCount(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error)
	GetClientCountByDeviceType(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error)
	GetClientCountByBrowserType(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error)
}
