package analytics

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"sync"
	"time"

	"littlebig/config"

	"github.com/sirupsen/logrus"
)

const analyticsStoreTimeoutFrac = 0.8

type PostgresDataCollector struct {
	db              *sql.DB
	tempHitDataList *[]HitData
	mutex           *sync.Mutex
	ticker          *time.Ticker
	stop            chan struct{}
	syncInterval    time.Duration
}

func NewPostgresDataCollector(db *sql.DB, syncInterval time.Duration) *PostgresDataCollector {
	collector := &PostgresDataCollector{
		db:              db,
		mutex:           &sync.Mutex{},
		ticker:          time.NewTicker(syncInterval),
		stop:            make(chan struct{}),
		tempHitDataList: &[]HitData{},
		syncInterval:    syncInterval,
	}

	return collector
}

func (dc *PostgresDataCollector) UrlHit(data HitData) {
	dc.mutex.Lock()
	*dc.tempHitDataList = append(*dc.tempHitDataList, data)
	dc.mutex.Unlock()
}

func (dc *PostgresDataCollector) StartSyncLoop() {
	for {
		select {
		case <-dc.ticker.C:
			dc.mutex.Lock()
			syncingData := dc.tempHitDataList
			dc.tempHitDataList = &[]HitData{}
			dc.mutex.Unlock()
			if len(*syncingData) == 0 {
				// if there is no data to persist, stop this time.
				continue
			}
			aggr := aggregateHitData(*syncingData)
			err := dc.storeAnalytics(aggr)
			if err != nil {
				logrus.WithError(err).Error("could not store analytics")
			} else {
				logrus.Info("analytics stored successfully")
			}
		case <-dc.stop:
			return
		}
	}
}

// storeAnalytics tries to bulk insert cached results with an expected precision
// into the database
func (dc *PostgresDataCollector) storeAnalytics(analytics AggregatedAnalyticsMap) error {
	ctx, cancel := context.WithTimeout(context.Background(),
		time.Duration(float64(dc.syncInterval)*analyticsStoreTimeoutFrac))
	defer cancel()
	timestamp := time.Now().Unix()
	valueStrings := make([]string, 0, len(analytics))
	valueArgs := make([]interface{}, 0, len(analytics)*6)
	positionalIndex := 1
	for hitData, count := range analytics {
		valueStrings = append(valueStrings,
			fmt.Sprintf("($%d, $%d, $%d, $%d, $%d, $%d)",
				positionalIndex,
				positionalIndex+1,
				positionalIndex+2,
				positionalIndex+3,
				positionalIndex+4,
				positionalIndex+5,
			))
		positionalIndex += 6
		valueArgs = append(valueArgs, hitData.ShortUrl)
		valueArgs = append(valueArgs, hitData.ClientAddr)
		valueArgs = append(valueArgs, hitData.Browser)
		valueArgs = append(valueArgs, hitData.DeviceType)
		valueArgs = append(valueArgs, count)
		valueArgs = append(valueArgs, timestamp)
	}
	stmt, err := dc.db.PrepareContext(ctx,
		fmt.Sprintf("insert into urlHit (shorturl, client, browser, device, hitcount, timestamp) values %s",
			strings.Join(valueStrings, ",")))
	fmt.Println(fmt.Sprintf("insert into urlHit (shorturl, client, browser, device, hitcount, timestamp) values %s",
		strings.Join(valueStrings, ",")))
	fmt.Println(valueArgs...)
	if err != nil {
		return err
	}
	_, err = stmt.ExecContext(ctx, valueArgs...)
	return err
}

func (dc *PostgresDataCollector) GetTotalUrlHitCount(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	var tableName string
	switch timeRange {
	case AnalyticsTimeRangeDaily:
		tableName = "urlHit_total_daily"
	case AnalyticsTimeRangeYesterday:
		tableName = "urlHit_total_yesterday"
	case AnalyticsTimeRangeWeekly:
		tableName = "urlHit_total_weekly"
	case AnalyticsTimeRangeMonthly:
		tableName = "urlHit_total_monthly"
	}
	query := fmt.Sprintf("select hitCount from %s where shortUrl=$1", tableName)
	rows, err := dc.db.QueryContext(ctx, query, shortUrl)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var aggr []FinalAggregatedAnalytics
	for rows.Next() {
		var hitCount int
		err := rows.Scan(&hitCount)
		if err != nil {
			return nil, err
		}
		aggr = append(aggr, FinalAggregatedAnalytics{
			ShortUrl: shortUrl,
			Count:    hitCount,
		})
	}
	return aggr, nil
}

func (dc *PostgresDataCollector) GetUrlHitCountByBrowser(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	var tableName string
	switch timeRange {
	case AnalyticsTimeRangeDaily:
		tableName = "urlHit_by_browser_daily"
	case AnalyticsTimeRangeYesterday:
		tableName = "urlHit_by_browser_yesterday"
	case AnalyticsTimeRangeWeekly:
		tableName = "urlHit_by_browser_weekly"
	case AnalyticsTimeRangeMonthly:
		tableName = "urlHit_by_browser_monthly"
	}
	query := fmt.Sprintf("select browser, hitCount from %s where shortUrl=$1", tableName)
	rows, err := dc.db.QueryContext(ctx, query, shortUrl)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var aggr []FinalAggregatedAnalytics
	for rows.Next() {
		var hitCount, browser int
		err := rows.Scan(&browser, &hitCount)
		if err != nil {
			return nil, err
		}
		aggr = append(aggr, FinalAggregatedAnalytics{
			ShortUrl: shortUrl,
			Browser:  &browser,
			Count:    hitCount,
		})
	}
	return aggr, nil
}

func (dc *PostgresDataCollector) GetUrlHitCountByDeviceType(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	var tableName string
	switch timeRange {
	case AnalyticsTimeRangeDaily:
		tableName = "urlHit_by_device_daily"
	case AnalyticsTimeRangeYesterday:
		tableName = "urlHit_by_device_yesterday"
	case AnalyticsTimeRangeWeekly:
		tableName = "urlHit_by_device_weekly"
	case AnalyticsTimeRangeMonthly:
		tableName = "urlHit_by_device_monthly"
	}
	query := fmt.Sprintf("select device, hitCount from %s where shortUrl=$1", tableName)
	rows, err := dc.db.QueryContext(ctx, query, shortUrl)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var aggr []FinalAggregatedAnalytics
	for rows.Next() {
		var hitCount, device int
		err := rows.Scan(&device, &hitCount)
		if err != nil {
			return nil, err
		}
		aggr = append(aggr, FinalAggregatedAnalytics{
			ShortUrl: shortUrl,
			Device:   &device,
			Count:    hitCount,
		})
	}
	return aggr, nil
}

func (dc *PostgresDataCollector) GetTotalClientCount(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	var tableName string
	switch timeRange {
	case AnalyticsTimeRangeDaily:
		tableName = "urlHit_by_client_daily"
	case AnalyticsTimeRangeYesterday:
		tableName = "urlHit_by_client_yesterday"
	case AnalyticsTimeRangeWeekly:
		tableName = "urlHit_by_client_weekly"
	case AnalyticsTimeRangeMonthly:
		tableName = "urlHit_by_client_monthly"
	}
	query := fmt.Sprintf("select hitCount from %s where shortUrl=$1", tableName)
	rows, err := dc.db.QueryContext(ctx, query, shortUrl)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var aggr []FinalAggregatedAnalytics
	for rows.Next() {
		var hitCount int
		err := rows.Scan(&hitCount)
		if err != nil {
			return nil, err
		}
		aggr = append(aggr, FinalAggregatedAnalytics{
			ShortUrl: shortUrl,
			Count:    hitCount,
		})
	}
	return aggr, nil
}

func (dc *PostgresDataCollector) GetClientCountByDeviceType(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	var tableName string
	switch timeRange {
	case AnalyticsTimeRangeDaily:
		tableName = "urlHit_by_client_device_daily"
	case AnalyticsTimeRangeYesterday:
		tableName = "urlHit_by_client_device_yesterday"
	case AnalyticsTimeRangeWeekly:
		tableName = "urlHit_by_client_device_weekly"
	case AnalyticsTimeRangeMonthly:
		tableName = "urlHit_by_client_device_monthly"
	}
	query := fmt.Sprintf("select device, hitCount from %s where shortUrl=$1", tableName)
	rows, err := dc.db.QueryContext(ctx, query, shortUrl)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var aggr []FinalAggregatedAnalytics
	for rows.Next() {
		var hitCount, device int
		err := rows.Scan(&device, &hitCount)
		if err != nil {
			return nil, err
		}
		aggr = append(aggr, FinalAggregatedAnalytics{
			ShortUrl: shortUrl,
			Device:   &device,
			Count:    hitCount,
		})
	}
	return aggr, nil
}

func (dc *PostgresDataCollector) GetClientCountByBrowserType(shortUrl string, timeRange AnalyticsTimeRange) ([]FinalAggregatedAnalytics, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	var tableName string
	switch timeRange {
	case AnalyticsTimeRangeDaily:
		tableName = "urlHit_by_client_browser_daily"
	case AnalyticsTimeRangeYesterday:
		tableName = "urlHit_by_client_browser_yesterday"
	case AnalyticsTimeRangeWeekly:
		tableName = "urlHit_by_client_browser_weekly"
	case AnalyticsTimeRangeMonthly:
		tableName = "urlHit_by_client_browser_monthly"
	}
	query := fmt.Sprintf("select browser, hitCount from %s where shortUrl=$1", tableName)
	rows, err := dc.db.QueryContext(ctx, query, shortUrl)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var aggr []FinalAggregatedAnalytics
	for rows.Next() {
		var hitCount, browser int
		err := rows.Scan(&browser, &hitCount)
		if err != nil {
			return nil, err
		}
		aggr = append(aggr, FinalAggregatedAnalytics{
			ShortUrl: shortUrl,
			Browser:  &browser,
			Count:    hitCount,
		})
	}
	return aggr, nil
}
