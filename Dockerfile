FROM golang:alpine AS builder
RUN mkdir /littlebig
ADD . /littlebig/
WORKDIR /littlebig
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o ./bin/littlebig ./cmd/littlebig/main.go

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root
COPY --from=builder /littlebig/bin/littlebig .
EXPOSE 8000
CMD ["./littlebig"]
