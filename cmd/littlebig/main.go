package main

import (
	"database/sql"
	"fmt"
	"net/http"
	"time"

	"littlebig/analytics"
	"littlebig/cache"
	"littlebig/config"
	"littlebig/handler"
	"littlebig/shortner"
	"littlebig/store"

	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

var mainConfig *config.LittleBigConfig

func main() {
	// setting up configs
	config.Setup()

	// extracting configs
	var err error
	mainConfig, err = config.Extract()
	if err != nil {
		log.WithError(err).Fatal("could not extract configs")
	}
	log.Debug("successfully extracted configs")

	// initializing components
	randomShortner, err := getUrlShortner()
	if err != nil {
		log.WithError(err).Fatal("could not initialize shortner")
	}
	log.Debug("successfully initialized shortner")

	urlStore, userStore, err := getStore()
	if err != nil {
		log.WithError(err).Fatal("could not initialize stores")
	}
	log.Debug("successfully initialized stores")

	urlCache, err := getUrlCache()
	if err != nil {
		log.WithError(err).Fatal("could not initialize cache")
	}
	log.Debug("successfully initialized cache")

	analyticsCollector, analyticsRetriever, err := getAnalyticsCollectorAndRetriever()
	if err != nil {
		log.WithError(err).Fatal("could not initialize analytics components")
	}
	log.Debug("successfully initialized analytics components")


	// registering handlers
	createUrlHandler := handler.NewCreateUrlHandler(randomShortner, urlStore)
	redirectUrlHandler := handler.NewRedirectUrlHandler(urlStore, urlCache, analyticsCollector)
	signupHandler := handler.NewSignupHandler(userStore)
	loginHandler := handler.NewLoginHandler(userStore)
	analyticsHandler := handler.NewAnalyticsHandler(urlStore, analyticsRetriever)

	// setting up routes
	r := mux.NewRouter()
	// public routes
	r.HandleFunc("/signup", signupHandler.Handle).Methods("POST")
	r.HandleFunc("/login", loginHandler.Handle).Methods("POST")
	r.HandleFunc("/{shortUrl}", redirectUrlHandler.Handle).Methods("GET")
	// protected routes
	protectedRouter := r.PathPrefix("/api").Subrouter()
	protectedRouter.Use(getJwtMiddleware().Handler)
	protectedRouter.HandleFunc("/create", createUrlHandler.Handle).Methods("POST")
	protectedRouter.HandleFunc("/analytics", analyticsHandler.Handle).Methods("POST")
	// registering router
	http.Handle("/", r)

	log.Info("starting HTTP server")
	// running HTTP server
	log.Fatal(http.ListenAndServe(mainConfig.ListenAddress, r))
}

func connectDB(host string, port int, user, password, dbname string) (*sql.DB, error) {
	connInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	return sql.Open("postgres", connInfo)
}

func getJwtMiddleware() *jwtmiddleware.JWTMiddleware {
	return jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return []byte(config.JwtKey), nil
		},
		SigningMethod: jwt.SigningMethodHS256,
	})
}

func getStore() (store.UrlStore, store.UserStore, error) {
	if mainConfig.UrlStore.Postgres != nil {
		pgConfig := mainConfig.UrlStore.Postgres
		db, err := connectDB(pgConfig.Host, pgConfig.Port, pgConfig.User, pgConfig.Password, pgConfig.Database)
		if err != nil {
			return nil, nil, err
		}
		st := store.NewPostgresStore(db)
		return st, st, nil
	} else if mainConfig.UrlStore.Memory != nil {
		st := store.NewMemoryUrlStore()
		return st, st, nil
	}
	return nil, nil, fmt.Errorf("no valid url store is provided")
}

func getUrlShortner() (shortner.Shortner, error) {
	if mainConfig.UrlShortner.Random != nil {
		return shortner.NewRandomShortner(time.Now().UnixNano(), 5), nil
	}
	return nil, fmt.Errorf("no valid shortner is provided")
}

func getUrlCache() (cache.UrlCache, error) {
	var cacheLayers []cache.UrlCache
	for _, layerConfig := range mainConfig.CacheLayers {
		if layerConfig.BigCache != nil {
			ttl := time.Second * time.Duration(layerConfig.BigCache.TTLSeconds)
			bigCacheLayer, err := cache.NewUrlBigCache(ttl)
			if err != nil {
				return nil, err
			}
			cacheLayers = append(cacheLayers, bigCacheLayer)
		} else if layerConfig.Redis != nil {
			var shards []cache.RedisCacheShard
			for _, shardConfig := range layerConfig.Redis.Shards {
				shards = append(shards, cache.RedisCacheShard{
					Name:    shardConfig.Name,
					Address: shardConfig.Address,
				})
			}
			ttl := time.Second * time.Duration(layerConfig.Redis.TTLSeconds)
			redisCache, err := cache.NewUrlRedisCache(shards, ttl)
			if err != nil {
				return nil, err
			}
			cacheLayers = append(cacheLayers, redisCache)
		} else {
			return nil, fmt.Errorf("invalid cache layer provided")
		}
	}
	if len(cacheLayers) == 0 {
		return nil, nil
	} else if len(cacheLayers) == 1 {
		return cacheLayers[0], nil
	} else {
		return cache.NewUrlMultiLayerCache(cacheLayers), nil
	}
}

func getAnalyticsCollectorAndRetriever() (analytics.DataCollector, analytics.AnalyticsRetriever, error) {
	if mainConfig.AnalyticsStore.Postgres != nil {
		pgConfig := mainConfig.AnalyticsStore.Postgres
		db, err := connectDB(pgConfig.Host, pgConfig.Port, pgConfig.User, pgConfig.Password, pgConfig.Database)
		if err != nil {
			return nil, nil, err
		}
		syncInterval := time.Second * time.Duration(pgConfig.SyncIntervalSeconds)
		pgAnalytics := analytics.NewPostgresDataCollector(db, syncInterval)
		go pgAnalytics.StartSyncLoop()
		return pgAnalytics, pgAnalytics, nil
	}
	return nil, nil, fmt.Errorf("no valid analytics collector is provided")
}