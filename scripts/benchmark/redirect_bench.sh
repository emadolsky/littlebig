#!/bin/bash
set -e
short_url=$1
timeout=$2

ab \
  -c 14 \
  -t $timeout \
  http://localhost:8000/"$short_url"

