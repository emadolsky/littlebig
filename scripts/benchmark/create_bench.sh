#!/bin/bash

set -e

token=$1
timeout=$2

ab \
  -p create_request.json \
  -T application/json \
  -H "Authorization: Bearer $token" \
  -c 2 \
  -t $timeout \
  http://localhost:8000/api/create

