#!/bin/bash

set -e
timeout=$1

RANDOM=$(echo -n "$(date +%s)" | tail -c 5)
username="user$((1 + RANDOM % 100))"

echo "$username"

curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"username": "'"$username"'","password": "emad","phoneNumber": "+989999999999","email": "emadolsky@gmail.com"}' \
  http://localhost:8000/signup

login_result=$( \
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"username": "'"$username"'","password": "emad"}' \
  http://localhost:8000/login \
    )

token=$(echo "$login_result" | jq '.token' -r)

shorten_result=$( \
  curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"longUrl":"http://127.0.0.1"}' \
  -H "Authorization: Bearer $token" \
  http://localhost:8000/api/create \
    )

short_url=$(echo "$shorten_result" | jq '.shortUrl' -r)

echo $token
echo $short_url

./create_bench.sh $token $timeout > ./results/create.result &
create_pid=$!
./redirect_bench.sh $short_url $timeout > ./results/redirect.result &
redirect_pid=$!
./analytics_bench.sh $token $short_url $timeout > ./results/analytics.result &
analytics_pid=$!

wait $create_pid
wait $redirect_pid
wait $analytics_pid

echo "-----------------------------------------------------------------------------"
echo "create results:"
cat ./results/create.result

echo "-----------------------------------------------------------------------------"
echo "redirect results:"
cat ./results/redirect.result

echo "-----------------------------------------------------------------------------"
echo "analytics results:"
cat ./results/analytics.result