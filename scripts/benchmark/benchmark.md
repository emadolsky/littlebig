# LittleBig Benchmark
For benchmarking, [ab](https://httpd.apache.org/docs/2.4/programs/ab.html) is used.

## Running Benchmark
In order to run benchmarks, you can:

> ./bench.sh <timeout>

where `timeout` is the amount of time you want benchmark to run in seconds.

Number of concurrent clients in each api is as wanted (5, 85, 10)

## Results
Results are not by any means, optimized. Since the requested performance is
already delivered, I did not spend time on optimizing and finding capacity of
the system.

The results on a `Lenovo X1 carbon 4th Gen. core i7 8GB` is like this:

```
create results:
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        
Server Hostname:        localhost
Server Port:            8000

Document Path:          /api/create
Document Length:        20 bytes

Concurrency Level:      2
Time taken for tests:   2.002 seconds
Complete requests:      435
Failed requests:        0
Total transferred:      59595 bytes
Total body sent:        154780
HTML transferred:       8700 bytes
Requests per second:    217.23 [#/sec] (mean)
Time per request:       9.207 [ms] (mean)
Time per request:       4.603 [ms] (mean, across all concurrent requests)
Transfer rate:          29.06 [Kbytes/sec] received
                        75.48 kb/s sent
                        104.55 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:     2    9   6.0      7      47
Waiting:        2    9   5.9      7      46
Total:          2    9   6.0      7      47

Percentage of the requests served within a certain time (ms)
  50%      7
  66%     10
  75%     12
  80%     14
  90%     17
  95%     21
  98%     24
  99%     28
 100%     47 (longest request)
-----------------------------------------------------------------------------
redirect results:
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        
Server Hostname:        localhost
Server Port:            8000

Document Path:          /FgCfG
Document Length:        43 bytes

Concurrency Level:      14
Time taken for tests:   2.001 seconds
Complete requests:      13478
Failed requests:        0
Non-2xx responses:      13478
Total transferred:      2614732 bytes
HTML transferred:       579554 bytes
Requests per second:    6736.17 [#/sec] (mean)
Time per request:       2.078 [ms] (mean)
Time per request:       0.148 [ms] (mean, across all concurrent requests)
Transfer rate:          1276.19 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    1   0.8      0      19
Processing:     0    2   1.6      1      50
Waiting:        0    1   1.4      1      50
Total:          0    2   1.7      2      50
WARNING: The median and mean for the initial connection time are not within a normal deviation
        These results are probably not that reliable.

Percentage of the requests served within a certain time (ms)
  50%      2
  66%      2
  75%      2
  80%      3
  90%      4
  95%      5
  98%      6
  99%      7
 100%     50 (longest request)
-----------------------------------------------------------------------------
analytics results:
This is ApacheBench, Version 2.3 <$Revision: 1706008 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        
Server Hostname:        localhost
Server Port:            8000

Document Path:          /api/analytics
Document Length:        16 bytes

Concurrency Level:      4
Time taken for tests:   2.002 seconds
Complete requests:      1240
Failed requests:        0
Total transferred:      164920 bytes
Total body sent:        428835
HTML transferred:       19840 bytes
Requests per second:    619.41 [#/sec] (mean)
Time per request:       6.458 [ms] (mean)
Time per request:       1.614 [ms] (mean, across all concurrent requests)
Transfer rate:          80.45 [Kbytes/sec] received
                        209.19 kb/s sent
                        289.64 kb/s total

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0       4
Processing:     1    6   5.2      4      49
Waiting:        1    6   5.2      4      49
Total:          1    6   5.3      5      49

Percentage of the requests served within a certain time (ms)
  50%      5
  66%      6
  75%      8
  80%     10
  90%     14
  95%     17
  98%     21
  99%     22
 100%     49 (longest request)
``` 

* Note: Redirect API is optimized with caches.