#!/bin/bash
set -e
token=$1
short_url=$2
timeout=$3

export short_url=$short_url
cat analytics_request.json | envsubst > analytics_request_sub.json

ab \
  -p analytics_request_sub.json \
  -T application/json \
  -H "Authorization: Bearer $token" \
  -c 4 \
  -t $timeout \
  http://localhost:8000/api/analytics

rm analytics_request_sub.json
