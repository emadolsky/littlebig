create table users
(
    userid      serial primary key,
    username    varchar(30),
    phoneNumber varchar(30),
    email       varchar(50),
    password    varchar(50),
    unique (username)
);

create table urls
(
    urlid    serial primary key,
    shortUrl varchar(20),
    longUrl  varchar(300),
    owner    integer,
    foreign key (owner) references users (userid),
    unique (shortUrl)
);

create table urlHit
(
    shortUrl  varchar(20),
    client    varchar(20),
    browser   integer,
    device    integer,
    hitCount  integer,
    timestamp integer
);

-- these indexes are for optimizing materialized view refreshes
create index urlHit_browser_index on urlHit (timestamp, shortUrl, browser);
create index urlHit_device_index on urlHit (timestamp, shortUrl, device);
create index urlHit_client_index on urlHit (timestamp, shortUrl, client);
create index urlHit_client_browser_index on urlHit (timestamp, shortUrl, client, browser);
create index urlHit_client_device_index on urlHit (timestamp, shortUrl, client, device);

-- daily views
create view urlHit_total_daily as
select shortUrl, sum(hitCount) as hitCount
from urlHit where timestamp >= (select extract(epoch from current_date))
group by (shortUrl);

create view urlHit_by_browser_daily as
select shortUrl, browser, sum(hitCount) as hitCount
from urlHit where timestamp >= (select extract(epoch from current_date))
group by (shortUrl, browser);

create view urlHit_by_device_daily as
select shortUrl, device, sum(hitCount) as hitCount
from urlHit where timestamp >= (select extract(epoch from current_date))
group by (shortUrl, device);

create view urlHit_by_client_daily as
select shortUrl, count(distinct client) as hitCount
from urlHit where timestamp >= (select extract(epoch from current_date))
group by (shortUrl);

create view urlHit_by_client_browser_daily as
select shortUrl, browser, count(distinct client) as hitCount
from urlHit where timestamp >= (select extract(epoch from current_date))
group by (shortUrl, browser);

create view urlHit_by_client_device_daily as
select shortUrl, device, count(distinct client) as hitCount
from urlHit where timestamp >= (select extract(epoch from current_date))
group by (shortUrl, device);
-- end of daily views


-- yesterday materialized views
create materialized view urlHit_total_yesterday as
select shortUrl, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '1 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl);
create index urlHit_total_yesterday_index on urlHit_total_yesterday(shortUrl);

create materialized view urlHit_by_browser_yesterday as
select shortUrl, browser, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '1 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, browser);
create index urlHit_by_browser_yesterday_index on urlHit_by_browser_yesterday(shortUrl);

create materialized view urlHit_by_device_yesterday as
select shortUrl, device, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '1 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, device);
create index urlHit_by_device_yesterday_index on urlHit_by_device_yesterday(shortUrl);


create materialized view urlHit_by_client_yesterday as
select shortUrl, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '1 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl);
create index urlHit_by_client_yesterday_index on urlHit_by_client_yesterday(shortUrl);

create materialized view urlHit_by_client_browser_yesterday as
select shortUrl, browser, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '1 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, browser);
create index urlHit_by_client_browser_yesterday_index on urlHit_by_client_browser_yesterday(shortUrl);

create materialized view urlHit_by_client_device_yesterday as
select shortUrl, device, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '1 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, device);
create index urlHit_by_client_device_yesterday_index on urlHit_by_client_device_yesterday(shortUrl);
-- end of yesterday materialized views

-- weekly materialized views
create materialized view urlHit_total_weekly as
select shortUrl, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '7 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl);
create index urlHit_total_weekly_index on urlHit_total_weekly(shortUrl);

create materialized view urlHit_by_browser_weekly as
select shortUrl, browser, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '7 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, browser);
create index urlHit_by_browser_weekly_index on urlHit_by_browser_weekly(shortUrl);

create materialized view urlHit_by_device_weekly as
select shortUrl, device, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '7 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, device);
create index urlHit_by_device_weekly_index on urlHit_by_device_weekly(shortUrl);

create materialized view urlHit_by_client_weekly as
select shortUrl, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '7 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl);
create index urlHit_by_client_weekly_index on urlHit_by_client_weekly(shortUrl);

create materialized view urlHit_by_client_browser_weekly as
select shortUrl, browser, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '7 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, browser);
create index urlHit_by_client_browser_weekly_index on urlHit_by_client_browser_weekly(shortUrl);

create materialized view urlHit_by_client_device_weekly as
select shortUrl, device, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '7 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, device);
create index urlHit_by_client_device_weekly_index on urlHit_by_client_device_weekly(shortUrl);
-- end of weekly materialized views


-- monthly materialized views
create materialized view urlHit_total_monthly as
select shortUrl, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '30 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl);
create index urlHit_total_monthly_index on urlHit_total_monthly(shortUrl);

create materialized view urlHit_by_browser_monthly as
select shortUrl, browser, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '30 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, browser);
create index urlHit_by_browser_monthly_index on urlHit_by_browser_monthly(shortUrl);

create materialized view urlHit_by_device_monthly as
select shortUrl, device, sum(hitCount) as hitCount
from urlHit where (select extract(epoch from current_date - interval '30 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, device);
create index urlHit_by_device_monthly_index on urlHit_by_device_monthly(shortUrl);

create materialized view urlHit_by_client_monthly as
select shortUrl, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '30 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl);
create index urlHit_by_client_monthly_index on urlHit_by_client_monthly(shortUrl);

create materialized view urlHit_by_client_browser_monthly as
select shortUrl, browser, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '30 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, browser);
create index urlHit_by_client_browser_monthly_index on urlHit_by_client_browser_monthly(shortUrl);

create materialized view urlHit_by_client_device_monthly as
select shortUrl, device, count(distinct client) as hitCount
from urlHit where (select extract(epoch from current_date - interval '30 day')) <= timestamp and timestamp < (select extract(epoch from current_date))
group by (shortUrl, device);
create index urlHit_by_client_device_monthly_index on urlHit_by_client_device_monthly(shortUrl);
-- end of monthly materialized views
