package store

type ConflictError struct {
	msg string
}

func NewConflictError(msg string) *ConflictError {
	return &ConflictError{
		msg: msg,
	}
}

func (e *ConflictError) Error() string {
	return e.msg
}

func IsConflictError(err error) bool {
	switch err.(type) {
	case *ConflictError:
		return true
	}
	return false
}

type NotFoundError struct {
	msg string
}

func NewNotFoundError(msg string) *NotFoundError {
	return &NotFoundError{
		msg: msg,
	}
}

func (e *NotFoundError) Error() string {
	return e.msg
}

func IsNotFoundError(err error) bool {
	switch err.(type) {
	case *NotFoundError:
		return true
	}
	return false
}