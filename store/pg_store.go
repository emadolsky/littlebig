package store

import (
	"context"
	"database/sql"
	"strings"

	"littlebig/config"

	_ "github.com/lib/pq"
)

type PostgresStore struct {
	db *sql.DB
}

func NewPostgresStore(db *sql.DB) *PostgresStore {
	return &PostgresStore{
		db: db,
	}
}

func (s *PostgresStore) SaveUrl(longUrl, shortUrl string, ownerId int) error {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	_, err := s.db.ExecContext(ctx,
		"insert into urls (shorturl, longurl, owner) values ($1, $2, $3)",
		shortUrl, longUrl, ownerId)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return NewConflictError("short-url already exists")
		}
		return err
	}
	return nil
}

func (s *PostgresStore) LoadUrl(shortUrl string) (longUrl string, ownerId int, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	row := s.db.QueryRowContext(ctx,
		"select longurl, owner from urls where shorturl = $1",
		shortUrl)
	err = row.Scan(&longUrl, &ownerId)
	if err != nil {
		if err == sql.ErrNoRows {
			return "", 0, NewNotFoundError("url not found")
		}
		return "", 0, err
	}
	return longUrl, ownerId, nil
}

func (s *PostgresStore) SaveUser(user User) error {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	_, err := s.db.ExecContext(ctx,
		"insert into users (username, phonenumber, email, password) values ($1, $2, $3, $4)",
		user.Username, user.PhoneNumber, user.Email, user.Password)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return NewConflictError("user already exists")
		}
		return err
	}
	return nil
}

func (s *PostgresStore) LoadUser(username string) (*User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), config.PostgresOperationTimeout)
	defer cancel()
	row := s.db.QueryRowContext(ctx,
		"select userid, email, phonenumber, password from users where username = $1",
		username)
	var userid int
	var email, phoneNumber, password string
	err := row.Scan(&userid, &email, &phoneNumber, &password)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, NewNotFoundError("url not found")
		}
		return nil, err
	}
	return &User{
		ID:          userid,
		Username:    username,
		Email:       email,
		Password:    password,
		PhoneNumber: phoneNumber,
	}, nil
}
