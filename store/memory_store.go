package store

import (
	"sync"
)

type storedUrl struct {
	long    string
	ownerId int
}

type MemoryStore struct {
	urlDB       map[string]storedUrl
	userDB      map[string]User
	urlRWMutex  *sync.Mutex
	userRWMutex *sync.Mutex
	userCount int
}

func NewMemoryUrlStore() *MemoryStore {
	return &MemoryStore{
		urlDB:       make(map[string]storedUrl),
		userDB:      make(map[string]User),
		urlRWMutex:  &sync.Mutex{},
		userRWMutex: &sync.Mutex{},
	}
}

func (s *MemoryStore) SaveUrl(longUrl, shortUrl string, ownerId int) error {
	s.urlRWMutex.Lock()
	defer s.urlRWMutex.Unlock()
	if _, exists := s.urlDB[shortUrl]; exists {
		return NewConflictError("short-url already exists")
	}
	s.urlDB[shortUrl] = storedUrl{
		long:    longUrl,
		ownerId: ownerId,
	}
	return nil
}

func (s *MemoryStore) LoadUrl(shortUrl string) (longUrl string, ownerId int, err error) {
	s.urlRWMutex.Lock()
	defer s.urlRWMutex.Unlock()
	var stUrl storedUrl
	var exists bool
	if stUrl, exists = s.urlDB[shortUrl]; !exists {
		return "", 0, NewNotFoundError("short-url does not exist")
	}
	return stUrl.long, stUrl.ownerId, nil
}

func (s *MemoryStore) SaveUser(user User) error {
	s.userRWMutex.Lock()
	defer s.userRWMutex.Unlock()
	if _, exists := s.userDB[user.Username]; exists {
		return NewConflictError("user already exists")
	}
	user.ID = s.userCount
	s.userCount++
	s.userDB[user.Username] = user
	return nil
}

func (s *MemoryStore) LoadUser(username string) (*User, error) {
	s.userRWMutex.Lock()
	defer s.userRWMutex.Unlock()
	var user User
	var exists bool
	if user, exists = s.userDB[username]; !exists {
		return nil, NewNotFoundError("user does not exist")
	}
	return &user, nil
}
