package store

type UrlStore interface {
	SaveUrl(longUrl, shortUrl string, ownerId int) error
	LoadUrl(shortUrl string) (longUrl string, ownerId int, err error)
}

type User struct {
	ID          int
	Username    string
	Email       string
	Password    string
	PhoneNumber string
}

type UserStore interface {
	SaveUser(user User) error
	LoadUser(username string) (*User, error)
}
