package shortner

import (
	"math/rand"
	"sync"

	"littlebig/config"
)

type RandomShortner struct {
	rand           *rand.Rand
	shortUrlLength int
	// because rand.Source is not thread-safe
	randMutex *sync.Mutex
}

func NewRandomShortner(seed int64, shortUrlLength int) *RandomShortner {
	return &RandomShortner{
		rand:           rand.New(rand.NewSource(seed)),
		shortUrlLength: shortUrlLength,
		randMutex: &sync.Mutex{},
	}
}

// RandomShortner GenerateShotUrl generates a short url randomly and with
// no use of longUrl
func (rs *RandomShortner) GenerateShortUrl(longUrl string) (string, error) {
	charSetLength := len(config.CharSet)
	shortUrl := ""
	rs.randMutex.Lock()
	for i := 0; i < rs.shortUrlLength; i++ {
		shortUrl += string(config.CharSet[rs.rand.Intn(charSetLength)])
	}
	rs.randMutex.Unlock()
	return shortUrl, nil
}
