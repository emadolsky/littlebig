package shortner

type Shortner interface {
	GenerateShortUrl(longUrl string) (string, error)
}

